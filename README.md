# SortViz

## Overview

**SortViz** is an application which visualizes variety of sorting algorithms on Atari 8-bit computer.

Please note that I created SortViz just for fun and the implementation is not optimized to be efficient or scalable.

At this point there are 20 different sorting algorithms included (in alphabetical order):
1. Bitonic sort,
1. Bubble sort,
1. Circle sort,
1. Coctail sort,
1. Comb sort,
1. Cycle sort,
1. Double selection sort,
1. Dual pivot quick sort,
1. Gnome sort,
1. Heap sort,
1. Insertion sort,
1. Merge sort,
1. Odd-Even sort,
1. Odd-Even merge sort,
1. Pancake sort,
1. Quick sort,
1. Radix sort,
1. Selection sort,
1. Shell sort,
1. Tim sort.

There are also available different methods for data shuffling.

SortViz shows each read and write access to the data during sorting or shuffling by green and red markers on the left and right side of the data area. Also the current number of read and write operations is shown. It is possible to change the speed of processing by decreasing or increasing the delay or even pause and resume the processing as well.

There are two ways to present the data by a set of horizontal lines which represent the stored numbers or by the image. You can exchange the view using **TAB** key. It is also possible to run all sorting algorithms in a sequence as a demo by pressing **Return** key.

You can see SortViz in action:\
[![SortViz WIP demo 1](https://img.youtube.com/vi/7zOZjgfawOk/0.jpg)](https://www.youtube.com/watch?v=7zOZjgfawOk)

[![SortViz WIP demo 2](https://img.youtube.com/vi/Y_kc8eS6a5k/0.jpg)](https://www.youtube.com/watch?v=Y_kc8eS6a5k)
