unit Operation;

interface

uses
  Core, Caption, Keyboard;

type
  TOperationKind =
  (
    okBubbleSort,
    okCoctailSort,
    okGnomeSort,
    okOddEvenSort,
    okSelectionSort,
    okDoubleSelectionSort,
    okInsertionSort,
    okCombSort,
    okShellSort,
    okMergeSort,
    okOddEvenMergeSort,
    okTimSort,
    okQuickSort,
    okDualPivotQuickSort,
    okHeapSort,
    okCycleSort,
    okCircleSort,
    okBitonicSort,
    okPancakeSort,
    okRadixSort,
    okKnuthShuffle,
    okFillAscendingWithShuffle,
    okFillAscending,
    okFillDescending,
    okFillPyramid,
    okFillInterlaced
  );

  TWorkingMode =
  (
    wmMainMenu,
    wmDemoSettings,
    wmProcessing,
    wmPaused,
    wmQuitQuery
  );

const
  OPERATION_COUNT = 26;
  MAX_OPERATION = OPERATION_COUNT - 1;
  
  OPERATION_CAPTIONS: array[0..MAX_OPERATION] of Byte = (
    capBubbleSort,
    capCoctailSort,
    capGnomeSort,
    capOddEvenSort,
    capSelectionSort,
    capDoubleSelectionSort,
    capInsertionSort,
    capCombSort,
    capShellSort,
    capMergeSort,
    capOddEvenMergeSort,
    capTimSort,
    capQuickSort,
    capDualPivotQuickSort,
    capHeapSort,
    capCycleSort,
    capCircleSort,
    capBitonicSort,
    capPancakeSort,
    capRadixSort,
    capKnuthShuffle,
    capFillAscendingWithShuffle,
    capFillAscending,
    capFillDescending,
    capFillPyramid,
    capFillInterlaced
  );

  OPERATION_SHORTCUTS: array[0..MAX_OPERATION] of Byte =(
    KEY_B,  //okBubbleSort
    KEY_C,  //okCoctailSort
    KEY_G,  //okGnomeSort
    KEY_V,  //okOddEvenSort
    KEY_S,  //okSelectionSort
    KEY_D,  //okDoubleSelectionSort
    KEY_I,  //okInsertionSort
    KEY_O,  //okCombSort
    KEY_E,  //okShellSort
    KEY_M,  //okMergeSort
    KEY_N,  //okOddEvenMergeSort
    KEY_W,  //okTimSort
    KEY_Q,  //okQuickSort
    KEY_U,  //okDualPivotQuickSort
    KEY_H,  //okHeapSort
    KEY_Y,  //okCycleSort
    KEY_L,  //okCircleSort
    KEY_T,  //okBitonicSort
    KEY_P,  //okPancakeSort
    KEY_R,  //okRadixSort
    KEY_1,  //okKnuthShuffle
    KEY_2,  //okFillAscendingWithShuffle
    KEY_3,  //okFillAscending
    KEY_4,  //okFillDescending
    KEY_5,  //okFillPyramid
    KEY_6   //okFillInterlaced
  );

  OPERATION_SHORTCUT_TEXTS: array[0..MAX_OPERATION] of Char =(
    'B'~*,  //okBubbleSort
    'C'~*,  //okCoctailSort
    'G'~*,  //okGnomeSort
    'V'~*,  //okOddEvenSort
    'S'~*,  //okSelectionSort
    'D'~*,  //okDoubleSelectionSort
    'I'~*,  //okInsertionSort
    'O'~*,  //okCombSort
    'E'~*,  //okShellSort
    'M'~*,  //okMergeSort
    'N'~*,  //okOddEvenMergeSort
    'W'~*,  //okTimSort
    'Q'~*,  //okQuickSort
    'U'~*,  //okDualPivotQuickSort
    'H'~*,  //okHeapSort
    'Y'~*,  //okCycleSort
    'L'~*,  //okCircleSort
    'T'~*,  //okBitonicSort
    'P'~*,  //okPancakeSort
    'R'~*,  //okRadixSort
    '1'~*,  //okKnuthShuffle
    '2'~*,  //okFillAscendingWithShuffle
    '3'~*,  //okFillAscending
    '4'~*,  //okFillDescending
    '5'~*,  //okFillPyramid
    '6'~*   //okFillInterlaced
  );

var
  workingMode: TWorkingMode;
  demo: Boolean;
  operationCount: Byte;
  operationIndex: Byte;
  operationKind: TOperationKind;
  demoShuffleMethod: TOperationKind;
  aborted: Boolean;
  quit: Boolean;

implementation

initialization
  workingMode := wmMainMenu;
  demo := False;
  aborted := False;
  quit := False;
end.