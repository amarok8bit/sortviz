unit TextUtils;

interface

uses
  Caption, Core;

const
  DIGIT_COUNT = 5;
  MAX_DIGIT = DIGIT_COUNT - 1;

var
  textPtr: PByte;
  textLen: Byte;

procedure PutCaption(caption: TCaption);
procedure PutCharToTextBuffer(c: Char);
procedure WriteNumber(numberPtr: PByte);

implementation

procedure PutCaption(caption: TCaption);
var
  len: Byte;
begin
  len := CAPTIONS_LENGTHS[caption];
  Move(Pointer(CAPTIONS[caption]), textPtr, len);
  Inc(textPtr, len);
  Inc(textLen, len);
end;

procedure PutCharToTextBuffer(c: Char);
begin
  textPtr^ := Byte(c);
  Inc(textPtr);
  Inc(textLen);
end;

procedure WriteNumber(numberPtr: PByte);
var
  i, tmp: Byte;
  ptr: PByte;
begin
  for i := 0 to MAX_DIGIT do
  begin
    tmp := numberPtr^;
    Inc(tmp, Ord('0'~));
    textPtr^ := tmp;
    Inc(numberPtr);
    Inc(textPtr);
  end;
end;

end.