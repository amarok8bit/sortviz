unit HelpScreen;

interface

function ShowHelp: Byte;

implementation

uses
  Keyboard, Core, Operation, Caption, TextUtils, Crt;

var
  pc0, pc1, pc2, pc3, c1: Byte;
  page: Byte;

procedure InitScreen;
begin
  pc0 := PCOLR0;
  pc1 := PCOLR1;
  pc2 := PCOLR2;
  pc3 := PCOLR3;
  c1 := COLOR1;
  PCOLR0 := 0;
  PCOLR1 := 0;
  PCOLR2 := 0;
  PCOLR3 := 0;
  COLOR1 := 0;
  SDLSTL := Word(HELP_DISPLAY_LIST_ADDR);
end;

const
  FADE_DELAY = 20;
  MAX_LUM = 12;

procedure FadeIn;
var
  i: Byte;
begin
  for i := 0 to MAX_LUM do
  begin
    COLOR1 := i;
    Delay(FADE_DELAY);
  end;
end;

procedure FadeOut;
var
  i: Byte;
begin
  for i := MAX_LUM downto 0 do
  begin
    COLOR1 := i;
    Delay(FADE_DELAY);
  end;
end;

procedure ShowScreen;
const
  TITLE_LEN: Byte = 30;
  TITLE_ADDR: Word = HELP_TEXT_ADDR + (HELP_TEXT_STEP - TITLE_LEN) div 2;
  NEXT_PAGE_LEN: Byte = 26;
  NEXT_PAGE_ADDR: Word = HELP_TEXT_ADDR + (HELP_TEXT_LINES - 1) * HELP_TEXT_STEP +
    (HELP_TEXT_STEP - NEXT_PAGE_LEN) div 2;
  FIRST_1: Byte = 0;
  LAST_1: Byte = MAX_OPERATION div 2;
  FIRST_2: Byte = LAST_1 + 1;
  LAST_2: Byte = MAX_OPERATION;
var
  i, len: Byte;
  op, first, last: Byte;
begin
  FillChar(Pointer(HELP_TEXT_ADDR), HELP_TEXT_SIZE, 0);

  textPtr := Pointer(TITLE_ADDR);
  PutCaption(TCaption(capHelpTitle));

  textPtr := Pointer(NEXT_PAGE_ADDR);
  PutCaption(TCaption(capHelpNextPage));

  if page = 0 then
  begin
    first := FIRST_1;
    last := LAST_1;
  end
  else begin
    first := FIRST_2;
    last := LAST_2;
  end;
  i := 2;
  for op := first to last do
  begin
    textPtr := Pointer(HELP_TEXT_ADDR);
    Inc(textPtr, i * HELP_TEXT_STEP + 5);
    Inc(i);

    PutCharToTextBuffer(Char($40));
    PutCharToTextBuffer(OPERATION_SHORTCUT_TEXTS[op]);
    PutCharToTextBuffer(Char($41));
    PutCaption(TCaption(OPERATION_CAPTIONS[op]));
  end;

  FadeIn;
end;

procedure HideScreen;
begin
  SDLSTL := Word(MAIN_DISPLAY_LIST_ADDR);
  PCOLR0 := pc0;
  PCOLR1 := pc1;
  PCOLR2 := pc2;
  PCOLR3 := pc3;
  COLOR1 := c1;
end;

procedure NextPage;
begin
  FadeOut;
  Inc(page);
  if page = 2 then page := 0;
  ShowScreen;
end;

function ShowHelp: Byte;
begin
  InitScreen;
  ShowScreen;

  repeat
    Result := GetKey;
    if Result = KEY_SPACE then
    begin
      NextPage;
    end;
  until Result <> KEY_SPACE;

  FadeOut;
  HideScreen;
end;

initialization
begin
  page := 0;
end;

end.